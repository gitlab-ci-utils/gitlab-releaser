'use strict';

/**
 * Functions for generating a GitLab release-cli script
 * for a release and saving to a file for execution.
 *
 * @module gitlab-releaser
 */

const fs = require('node:fs');
const path = require('node:path');
const logger = require('ci-logger');
const {
    changelogKeyword,
    usesChangelog,
    processChangelogData
} = require('./lib/changelog');
const { validateSchema } = require('./lib/schema');
const { formatAssetLink } = require('./lib/assets');

/**
 * @typedef  {object}  ReleaseOptions
 * @property {boolean} allowEmptyChangelogDesc Allow an empty description from the CHANGELOG.
 * @property {boolean} allowEmptyVariables     Allow an environment variable that cannot be
 *                                             expanded.
 * @property {string}  changelogPath           The path to the CHANGELOG directory or file.
 * @property {string}  releaseDirectory        The directory where the release file
 *                                             is located.
 * @property {string}  releaseFileName         The release filename.
 * @property {string}  releaseName             The name to use to find release data.
 */

/**
 * Gets a ci-logger formatted message object for error with exit code 1.
 *
 * @param   {string} message The error message to log.
 * @returns {object}         A ci-logger formatted message object.
 * @private
 */
const getErrorLogEntry = (message) => ({
    errorCode: 1,
    exitOnError: true,
    level: logger.Levels.Error,
    message
});

/**
 * Checks for a valid release name and logs error and exits if not specified.
 *
 * @param {string} releaseName The given release name.
 * @param {string} message     The error message if no release specified.
 * @private
 */
const validateReleaseName = (releaseName, message) => {
    if (!releaseName) {
        logger.log(getErrorLogEntry(message));
    }
};

/**
 * Checks for environment variables in the given value and expands them, if
 * found. Changelog values are ignored. If the variable has no value, it will
 * log error and exit.
 *
 * @param   {string}  value      The variable value.
 * @param   {boolean} allowEmpty Whether to allow empty variables.
 * @returns {string}             The expanded variable value.
 * @private
 */
const expandEnvironmentVariables = (value, allowEmpty) => {
    if (value === changelogKeyword) {
        return value;
    }

    const regex = /(?<completeVariable>\${?(?<variableName>[\dA-Z_]+)}?)/g;
    let newValue = value;

    const matches = newValue.matchAll(regex);
    for (const match of matches) {
        const { completeVariable, variableName } = match.groups;
        const variableValue = process.env[variableName];
        if (variableValue) {
            newValue = newValue.replace(completeVariable, variableValue);
            logger.log({
                message: `Expanded environment variable "${variableName}" to "${variableValue}"`
            });
        } else if (allowEmpty) {
            logger.log({
                message: `Environment variable "${variableName}" was not expanded`
            });
        } else {
            logger.log(
                getErrorLogEntry(
                    `Environment variable "${variableName}" was not found`
                )
            );
        }
    }
    return newValue;
};

/**
 * Expands all environment variables in the given release object.
 *
 * @param   {object}         release The release object.
 * @param   {ReleaseOptions} options The options used to create the release.
 * @returns {object}                 The updated release object.
 * @private
 */
const expandAllEnvironmentVariables = (release, options) => {
    for (const [key, value] of Object.entries(release)) {
        if (typeof value === 'string') {
            release[key] = expandEnvironmentVariables(
                value,
                options.allowEmptyVariables
            );
        } else if (Array.isArray(value)) {
            release[key] = value.map((item) =>
                expandEnvironmentVariables(item, options.allowEmptyVariables)
            );
        } else if (key === 'assets') {
            release[key].links = value.links.map((assetLink) => {
                // Only process existing entries
                for (const [assetKey, assetValue] of Object.entries(
                    assetLink
                )) {
                    assetLink[assetKey] = expandEnvironmentVariables(
                        assetValue,
                        options.allowEmptyVariables
                    );
                }
                return assetLink;
            });
        }
    }
    return release;
};

/**
 * Processes release to get CHANGELOG data, format asset links, and rename
 * properties to match release-cli schema.
 *
 * @param   {object}         release The release object.
 * @param   {ReleaseOptions} options The options used to create the release.
 * @returns {object}                 The release updated with CHANGELOG data.
 * @private
 */
// eslint-disable-next-line complexity -- allow low threshold
const processReleaseData = (release, options) => {
    // Process changelog data
    let processedRelease = expandAllEnvironmentVariables(release, options);

    processedRelease = usesChangelog(processedRelease)
        ? processChangelogData(processedRelease, options)
        : processedRelease;

    // Format an rename asset links
    if (processedRelease?.assets?.links) {
        const { links } = processedRelease.assets;
        processedRelease['assets-link'] = links.map((assetLink) =>
            formatAssetLink(assetLink)
        );
        delete processedRelease.assets;
    }

    // Rename properties to match release-cli schema
    if (processedRelease?.milestones) {
        processedRelease.milestone = processedRelease.milestones;
        delete processedRelease.milestones;
    }
    if (processedRelease?.tag_name) {
        processedRelease['tag-name'] = processedRelease.tag_name;
        delete processedRelease.tag_name;
    }
    if (processedRelease?.released_at) {
        processedRelease['released-at'] = processedRelease.released_at;
        delete processedRelease.released_at;
    }

    return processedRelease;
};

/**
 * Gets a release object from a gitlab-releaser file for a given release.
 *
 * @param   {object} data        The gitlab-releaser data.
 * @param   {string} releaseName The given release name.
 * @returns {object}             The applicable release object.
 * @private
 */
const getReleaseFromGitLabReleaser = (data, releaseName) => {
    const releaseData =
        data.releases && Object.keys(data.releases).includes(releaseName)
            ? data.releases[releaseName]
            : {};
    return { ...data.defaults, ...releaseData };
};

/**
 * Generates GitLab release object for the given release. Will log error and
 * exit the process if invalid arguments or data is provided.
 *
 * @param   {ReleaseOptions} options The options used to create the release.
 * @returns {object}                 The release-cli release object.
 * @static
 * @public
 */
const getReleaseData = (options) => {
    const releaseFile = path.join(
        // User inputs to dev tool, no limits on file path.
        // nosemgrep: path-join-resolve-traversal
        options.releaseDirectory,
        // nosemgrep: path-join-resolve-traversal
        options.releaseFileName
    );
    // nosemgrep: detect-non-literal-fs-filename, eslint.detect-non-literal-fs-filename
    const data = JSON.parse(fs.readFileSync(releaseFile));

    if (!validateSchema(data)) {
        logger.log(
            getErrorLogEntry(
                `"${releaseFile}" is an invalid gitlab-releaser file`
            )
        );
    }
    validateReleaseName(options.releaseName, 'Release must be specified');
    const release = getReleaseFromGitLabReleaser(data, options.releaseName);
    return processReleaseData(release, options);
};

/**
 * Saves the given release data as a release-cli compatible release file with
 * the specified file name in the specified folder. Will exit the process on
 * error.
 *
 * @param {string} directory       The destination directory to save
 *                                 the release file.
 * @param {string} releaseFileName The file name for the release.
 * @param {object} releaseData     The release data object.
 * @static
 * @public
 */
const saveReleaseFile = (directory, releaseFileName, releaseData) => {
    try {
        // User inputs to dev tool, no limits on file path.
        // nosemgrep: path-join-resolve-traversal
        const releaseScriptFile = path.join(directory, releaseFileName);
        // User inputs to dev tool, no limits on file path.
        // nosemgrep: detect-non-literal-fs-filename, eslint.detect-non-literal-fs-filename
        fs.writeFileSync(releaseScriptFile, JSON.stringify(releaseData));
        logger.log({ message: `Saved file "${releaseScriptFile}"` });
    } catch (error) {
        logger.log(getErrorLogEntry(`Error saving file\n${error.message}`));
    }
};

module.exports.getReleaseData = getReleaseData;
module.exports.saveReleaseFile = saveReleaseFile;
