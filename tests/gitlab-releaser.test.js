'use strict';

const fs = require('node:fs');
const path = require('node:path');
const { bin } = require('bin-tester');

const removeReleaseFile = (fileName) => {
    if (fs.existsSync(fileName)) {
        fs.unlinkSync(fileName);
    }
};

describe('gitlab-releaser', () => {
    const directory = '.gitlab';
    const releaseFileName = 'release.json';
    const scriptFileName = path.join(directory, releaseFileName);

    beforeAll(() => {
        removeReleaseFile(scriptFileName);
    });

    afterEach(() => {
        removeReleaseFile(scriptFileName);
    });

    it('should process gitlab-releaser.json, write the expected file, and log results', async () => {
        expect.assertions(3);
        const releaseName = '0.5.0';
        const binArguments = ['-r', releaseName];
        const environment = { CI_COMMIT_TAG: releaseName };

        const results = await bin({ binArguments, environment });

        expect(results.code).toBe(0);
        expect(results.stdout).toMatch('Saved file');
        expect(fs.existsSync(scriptFileName)).toBe(true);
    });

    it('should default to release CI_COMMIT_TAG if not specified', async () => {
        expect.assertions(3);
        const environment = { CI_COMMIT_TAG: '0.5.0' };

        const results = await bin({ environment });

        expect(results.code).toBe(0);
        expect(results.stdout).toMatch(environment.CI_COMMIT_TAG);
        expect(fs.existsSync(scriptFileName)).toBe(true);
    });

    it('should use changelog path if specified', async () => {
        expect.assertions(3);
        const releaseName = '0.0.1';
        const changelogPath = path.join('tests', 'fixtures', 'test-project');
        const binArguments = ['-r', releaseName, '-c', changelogPath];
        const environment = { CI_COMMIT_TAG: releaseName };

        const results = await bin({
            binArguments,
            environment
        });

        expect(results.code).toBe(0);
        expect(results.stdout).toMatch(changelogPath);
        expect(fs.existsSync(scriptFileName)).toBe(true);
    });

    it('should allow empty changelog descriptions if specified', async () => {
        expect.assertions(3);
        const changelogPath = path.join('tests', 'fixtures', 'changelogs');
        const releaseName = '1.0.0';
        const binArguments = ['-r', releaseName, '-c', changelogPath, '-a'];
        const environment = { CI_COMMIT_TAG: releaseName };

        const results = await bin({ binArguments, environment });

        expect(results.code).toBe(0);
        expect(results.stdout).toMatch(
            'Retrieved release description from changelog'
        );
        expect(fs.existsSync(scriptFileName)).toBe(true);
    });

    it('should allow empty variables if specified and variables have no value', async () => {
        expect.assertions(2);
        const releaseName = '0.0.1';
        const workingDirectory = path.join('tests', 'fixtures', 'test-project');
        const binArguments = ['-r', releaseName, '-v'];
        // Explicitly set to empty value to override value set on tag pipelines
        const environment = { CI_COMMIT_TAG: '' };

        const results = await bin({
            binArguments,
            environment,
            workingDirectory
        });

        expect(results.code).toBe(0);
        expect(results.stdout).toMatch('"CI_COMMIT_TAG" was not expanded');
    });

    it('should log error and exit if release not specified and CI_COMMIT_TAG is not specified', async () => {
        expect.assertions(3);
        // Explicitly set to empty value to override value set on tag pipelines
        const environment = { CI_COMMIT_TAG: '' };

        const results = await bin({ environment });

        expect(results.code).toBe(1);
        expect(results.stderr).toMatch('Release must be specified');
        expect(fs.existsSync(scriptFileName)).toBe(false);
    });

    it('should log error and exit if release uses changelog and description is empty', async () => {
        expect.assertions(3);
        const changelogPath = path.join('tests', 'fixtures', 'changelogs');
        const releaseName = '1.0.0';
        const binArguments = ['-r', releaseName, '-c', changelogPath];
        const environment = { CI_COMMIT_TAG: releaseName };

        const results = await bin({ binArguments, environment });

        expect(results.code).toBe(1);
        expect(results.stderr).toMatch(
            `No description found for release name "${releaseName}"`
        );
        expect(fs.existsSync(scriptFileName)).toBe(false);
    });
});
