'use strict';

const fs = require('node:fs');
const path = require('node:path');
const { validateSchema } = require('../lib/schema');

const testDirectory = 'tests';
const testCaseDirectory = 'fixtures';
const testFiles = [
    {
        fileName: 'gitlab-releaser-valid-all-values.json',
        validity: 'valid'
    },
    {
        fileName: 'gitlab-releaser-valid-override-milestones.json',
        validity: 'valid'
    },
    {
        fileName: 'gitlab-releaser-valid-none.json',
        validity: 'valid'
    },
    {
        fileName: 'gitlab-releaser-invalid-default-extra-property.json',
        validity: 'invalid'
    },
    {
        fileName: 'gitlab-releaser-invalid-release-asset-link.json',
        validity: 'invalid'
    }
];

describe('is valid gitlab-releaser schema', () => {
    it.each(testFiles)(
        `should find that file '$fileName' is $validity`,
        ({ fileName, validity }) => {
            expect.assertions(1);
            const data = JSON.parse(
                fs.readFileSync(
                    path.join(
                        testDirectory,
                        testCaseDirectory,
                        'gitlab-releaser',
                        fileName
                    )
                )
            );
            expect(validateSchema(data)).toStrictEqual(validity === 'valid');
        }
    );
});
