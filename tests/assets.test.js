'use strict';

const { formatAssetLink } = require('../lib/assets.js');

const assetLinkMinimum = {
    name: 'foo',
    url: 'https://example.com/foo'
};

const assetLinkAll = {
    // eslint-disable-next-line camelcase -- match API
    direct_asset_path: '/bar.tar.gz',
    name: 'bar',
    type: 'package',
    url: 'https://example.com/bar'
};

const checkProperty = (assetLink, property, hasProperty = true) => {
    const result = JSON.parse(formatAssetLink(assetLink));
    if (hasProperty) {
        expect.assertions(2);
        expect(result).toHaveProperty(property);
        expect(result[property]).toBe(assetLink[property]);
    } else {
        expect.assertions(1);
        expect(result).not.toHaveProperty(property);
    }
};

describe('formatAssetLink', () => {
    it('should return a valid JSON string', () => {
        expect.assertions(1);
        const result = formatAssetLink(assetLinkMinimum);
        expect(() => JSON.parse(result)).not.toThrow();
    });

    it('should throw if no value provided', () => {
        expect.assertions(1);
        expect(() => formatAssetLink()).toThrow(TypeError);
    });

    it('should contain a `name` property', () => {
        expect.hasAssertions();
        checkProperty(assetLinkMinimum, 'name');
    });

    it('should contain a `url` property', () => {
        expect.hasAssertions();
        checkProperty(assetLinkMinimum, 'url');
    });

    it('should contain a `type` property if one was provided', () => {
        expect.hasAssertions();
        checkProperty(assetLinkAll, 'type');
    });

    it('should not contain a `type` property if one was not provided', () => {
        expect.hasAssertions();
        checkProperty(assetLinkMinimum, 'type', false);
    });

    it('should contain a `direct_asset_path` property if one was provided', () => {
        expect.hasAssertions();
        checkProperty(assetLinkAll, 'direct_asset_path');
    });

    it('should not contain a `direct_asset_path` property if one was not provided', () => {
        expect.hasAssertions();
        checkProperty(assetLinkMinimum, 'direct_asset_path', false);
    });
});
