# Changelog

## v1.0.0 (2020-11-21)

## Changed

- Changed some function to do something else.

### Fixed

- Updated to latest dependencies

## v0.3.0 (2020-11-20)

Initial implementation
