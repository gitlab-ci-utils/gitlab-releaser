'use strict';

const fs = require('node:fs');
const path = require('node:path');
const { getReleaseData, saveReleaseFile } = require('..');

describe('get release data', () => {
    const testDirectory = 'tests';
    const testCaseDirectory = 'fixtures';
    const gitlabReleaserTestFileDirectory = path.join(
        testDirectory,
        testCaseDirectory,
        'gitlab-releaser'
    );
    const defaultValidGitlabReleaserFileName =
        'gitlab-releaser-valid-all-values.json';
    const gitlabReleaserOnlyDefaultsFileName =
        'gitlab-releaser-valid-only-defaults.json';
    const gitlabReleaserOnlyRelease010FileName =
        'gitlab-releaser-valid-only-010.json';
    const gitlabReleaserOverrideMilestones =
        'gitlab-releaser-valid-override-milestones.json';
    const gitlabReleaserNoTagFileName = 'gitlab-releaser-valid-no-tag.json';
    const gitlabReleaserVariablesAsset =
        'gitlab-releaser-valid-variables-asset.json';
    const gitlabReleaserVariablesAssetMultiple =
        'gitlab-releaser-valid-variables-asset-multiple.json';
    const gitlabReleaserVariablesMilestones =
        'gitlab-releaser-valid-variables-milestones.json';
    const gitlabReleaserVariablesMilestonesMultiple =
        'gitlab-releaser-valid-variables-milestones-multiple.json';
    const gitlabReleaserVariablesString =
        'gitlab-releaser-valid-variables-string.json';
    const gitlabReleaserVariablesStringMultiple =
        'gitlab-releaser-valid-variables-string-multiple.json';
    const gitlabReleaserVariablesSingle =
        'gitlab-releaser-valid-variables-single.json';
    const gitlabReleaserReleaseTagMismatchFileName =
        'gitlab-releaser-valid-release-tag-mismatch.json';
    const defaultReleaseName = '0.5.0';
    const defaultChangelogPath = '.';
    const emptyChangelogPath = path.join('tests', 'fixtures', 'changelogs');

    const defaultGitLabReleaserCliOptions = {
        allowEmptyChangelogDesc: false,
        allowEmptyVariables: false,
        changelogPath: defaultChangelogPath,
        releaseDirectory: gitlabReleaserTestFileDirectory,
        releaseFileName: defaultValidGitlabReleaserFileName,
        releaseName: defaultReleaseName
    };

    beforeEach(() => {
        // Add mock to catch console output
        jest.spyOn(console, 'log').mockImplementation(() => {});
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should pull name value from CHANGELOG if specified', () => {
        expect.assertions(1);
        const expectedName = 'v0.5.0 (2020-11-10)';

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions
        });

        expect(result.name).toBe(expectedName);
    });

    it('should pull name value from gitlab-releaser if CHANGELOG not specified', () => {
        expect.assertions(1);
        const expectedName = 'v0.2.0 (2020-11-01)';

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseName: '0.2.0'
        });

        expect(result.name).toBe(expectedName);
    });

    it('should pull description value from CHANGELOG if specified', () => {
        expect.assertions(1);
        const expectedDescription = 'Initial implementation';

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions
        });

        expect(result.description).toBe(expectedDescription);
    });

    it('should pull description value from gitlab-releaser if CHANGELOG not specified', () => {
        expect.assertions(1);
        const expectedDescription = 'Second release';

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseName: '0.2.0'
        });

        expect(result.description).toBe(expectedDescription);
    });

    it('should allow empty description value from CHANGELOG if specified and empty allowed', () => {
        expect.assertions(1);
        const releaseName = '1.0.0';
        const changelogPath = emptyChangelogPath;
        const allowEmptyChangelogDesc = true;

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            allowEmptyChangelogDesc,
            changelogPath,
            releaseName
        });

        expect(result).toMatchSnapshot();
    });

    it('should convert `assets.links` object to array of JSON strings', () => {
        expect.assertions(2);

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseName: '0.1.0'
        });

        expect(result['assets-link']).toBeInstanceOf(Array);
        expect(() => JSON.parse(result['assets-link'][0])).not.toThrow();
    });

    it('should rename `milestones` to `milestone` if specified', () => {
        expect.assertions(3);
        const releaseFileName = gitlabReleaserOverrideMilestones;
        const releaseName = '2.0.0';
        const expectedMilestone = ['v2.0.0'];

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseFileName,
            releaseName
        });

        expect(result).not.toHaveProperty('milestones');
        expect(result).toHaveProperty('milestone');
        expect(result.milestone).toStrictEqual(expectedMilestone);
    });

    it('should rename `released_at` to `released-at` if specified', () => {
        expect.assertions(3);
        const releaseName = '0.1.0';
        const expectedReleasedAt = '2020-10-28T05:00:00Z';
        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseName
        });

        expect(result).not.toHaveProperty('released_at');
        expect(result).toHaveProperty('released-at');
        expect(result['released-at']).toStrictEqual(expectedReleasedAt);
    });

    it('should return release defaults if release is not found in the gitlab-releaser.json file', () => {
        expect.assertions(1);
        const releaseFileName = gitlabReleaserOnlyRelease010FileName;

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseFileName
        });

        expect(result).toMatchSnapshot();
    });

    it('should return release defaults if gitlab-releaser.json file has no releases', () => {
        expect.assertions(1);
        const releaseFileName = gitlabReleaserOnlyDefaultsFileName;

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseFileName
        });

        expect(result).toMatchSnapshot();
    });

    it('should not include tag-name if not specified in release', () => {
        expect.assertions(1);
        const releaseName = '0.2.0';
        const releaseFileName = gitlabReleaserNoTagFileName;

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseFileName,
            releaseName
        });

        expect(result).toMatchSnapshot();
    });

    it('should include tag-name from release if specified', () => {
        expect.assertions(1);
        const releaseName = '0.3.0';
        const releaseFileName = gitlabReleaserReleaseTagMismatchFileName;

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseFileName,
            releaseName
        });

        expect(result).toMatchSnapshot();
    });

    it('should expand environment variables with values in release string properties', () => {
        expect.assertions(1);
        const releaseFileName = gitlabReleaserVariablesString;
        const expectedResult = {
            description: 'This is a release',
            name: 'A release',
            ref: '123456789',
            'released-at': 'Now',
            'tag-name': 'v4.5.6'
        };
        process.env.RELEASE_DESCRIPTION = expectedResult.description;
        process.env.RELEASE_NAME = expectedResult.name;
        process.env.RELEASE_REF = expectedResult.ref;
        process.env.RELEASE_RELEASED_AT = expectedResult['released-at'];
        process.env.RELEASE_TAG = expectedResult['tag-name'];

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseFileName
        });

        expect(result).toStrictEqual(expectedResult);

        delete process.env.RELEASE_DESCRIPTION;
        delete process.env.RELEASE_NAME;
        delete process.env.RELEASE_REF;
        delete process.env.RELEASE_RELEASED_AT;
        delete process.env.RELEASE_TAG;
    });

    it('should expand environment variables with multiple values in release string properties', () => {
        expect.assertions(1);
        const releaseFileName = gitlabReleaserVariablesStringMultiple;
        const releaseName = 'A release';
        const releaseTime = 'Now';
        const releaseDate = 'Today';
        const expectedResult = {
            name: `${releaseName} released at ${releaseTime} on ${releaseDate}`
        };
        process.env.RELEASE_NAME = releaseName;
        process.env.RELEASE_TIME = releaseTime;
        process.env.RELEASE_DATE = releaseDate;

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseFileName
        });

        expect(result.name).toBe(expectedResult.name);

        delete process.env.RELEASE_NAME;
        delete process.env.RELEASE_TIME;
        delete process.env.RELEASE_DATE;
    });

    it('should expand environment variables with values in release milestone values', () => {
        expect.assertions(1);
        const releaseFileName = gitlabReleaserVariablesMilestones;
        const expectedResult = {
            milestones: ['1.2.3', '3.2.1']
        };
        process.env.CI_COMMIT_TAG = expectedResult.milestones[0];
        process.env.CI_COMMIT_TAG_2 = expectedResult.milestones[1];

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseFileName
        });

        expect(result.milestone).toStrictEqual(expectedResult.milestones);

        delete process.env.CI_COMMIT_TAG;
        delete process.env.CI_COMMIT_TAG_2;
    });

    it('should expand environment variables with multiple values in release milestone values', () => {
        expect.assertions(1);
        const releaseFileName = gitlabReleaserVariablesMilestonesMultiple;
        const releaseTagPrefix = 'v';
        const releaseTag = '3.2.1';
        const releaseTagSuffix = '-rc';
        const expectedResult = `${releaseTagPrefix}${releaseTag}${releaseTagSuffix}`;
        process.env.RELEASE_TAG_PREFIX = releaseTagPrefix;
        process.env.RELEASE_TAG = releaseTag;
        process.env.RELEASE_TAG_SUFFIX = releaseTagSuffix;

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseFileName
        });

        expect(result.milestone[0]).toBe(expectedResult);

        delete process.env.RELEASE_TAG_PREFIX;
        delete process.env.RELEASE_TAG;
        delete process.env.RELEASE_TAG_SUFFIX;
    });

    it('should expand environment variables with values in release asset values', () => {
        expect.assertions(1);
        const releaseFileName = gitlabReleaserVariablesAsset;
        const expectedResult = {
            assets: {
                links: [
                    {
                        name: 'The link name',
                        url: 'http://not.a.link.test',
                        /* eslint-disable-next-line sort-keys, camelcase --
                           match order and API */
                        direct_asset_path: 'The path'
                        // Type is excluded since limited to valid enumerations
                    }
                ]
            }
        };
        const expectedAssetLink = [
            JSON.stringify(expectedResult.assets.links[0])
        ];

        process.env.RELEASE_ASSET_NAME = expectedResult.assets.links[0].name;
        process.env.RELEASE_ASSET_URL = expectedResult.assets.links[0].url;
        process.env.RELEASE_DIRECT_ASSET_PATH =
            expectedResult.assets.links[0].direct_asset_path;

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseFileName
        });

        expect(result['assets-link']).toStrictEqual(expectedAssetLink);

        delete process.env.RELEASE_ASSET_NAME;
        delete process.env.RELEASE_ASSET_URL;
        delete process.env.RELEASE_DIRECT_ASSET_PATH;
    });

    it('should expand environment variables with multiple values in release asset values', () => {
        expect.assertions(1);
        const releaseFileName = gitlabReleaserVariablesAssetMultiple;
        const assetUrlRoot = 'http://somewhere.test';
        const assetUrlPath = 'foo/bar';
        const assetUrlParameter = 'baz=2';
        const expectedResult = {
            assets: {
                links: [
                    {
                        name: 'Download',
                        url: `${assetUrlRoot}/${assetUrlPath}?${assetUrlParameter}`
                    }
                ]
            }
        };
        const expectedAssetLink = [
            JSON.stringify(expectedResult.assets.links[0])
        ];

        process.env.RELEASE_ASSET_URL_ROOT = assetUrlRoot;
        process.env.RELEASE_ASSET_URL_PATH = assetUrlPath;
        process.env.RELEASE_ASSET_URL_PARAM = assetUrlParameter;

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            releaseFileName
        });

        expect(result['assets-link']).toStrictEqual(expectedAssetLink);

        delete process.env.RELEASE_ASSET_URL_ROOT;
        delete process.env.RELEASE_ASSET_URL_PATH;
        delete process.env.RELEASE_ASSET_URL_PARAM;
    });

    it('should allow empty variables not expanded if allowed and variables are empty', () => {
        expect.assertions(1);
        const releaseName = '1.0.0';
        const releaseFileName = gitlabReleaserVariablesString;
        const allowEmptyVariables = true;

        const result = getReleaseData({
            ...defaultGitLabReleaserCliOptions,
            allowEmptyVariables,
            releaseFileName,
            releaseName
        });

        expect(result.ref).toBe('$RELEASE_REF');
    });

    const invalidInputTest = (options, errorMessage) => {
        expect.assertions(3);

        const mockError = 'Mock';
        const consoleErrorSpy = jest
            .spyOn(console, 'error')
            .mockImplementation(() => {});
        const processExitSpy = jest
            .spyOn(process, 'exit')
            .mockImplementation(() => {
                throw new Error(mockError);
            });

        // This command would normally call process.exit, but mocked with error for test,
        // so expect that mock error instead
        expect(() => getReleaseData(options)).toThrow(mockError);

        expect(consoleErrorSpy.mock.calls[0][0]).toMatch(errorMessage);
        expect(processExitSpy).toHaveBeenCalledWith(1);
    };

    it('should log error and exit for environment variables without values in release string properties', () => {
        expect.hasAssertions();
        const releaseFileName = gitlabReleaserVariablesSingle;
        const variable = 'RELEASE_NAME';

        invalidInputTest(
            {
                ...defaultGitLabReleaserCliOptions,
                releaseFileName
            },
            `Environment variable "${variable}" was not found`
        );
    });

    it('should log error and exit for environment variables without values in release array values', () => {
        expect.hasAssertions();
        const releaseFileName = gitlabReleaserVariablesMilestones;
        const variable = 'CI_COMMIT_TAG';

        invalidInputTest(
            {
                ...defaultGitLabReleaserCliOptions,
                releaseFileName
            },
            `Environment variable "${variable}" was not found`
        );
    });

    it('should log error and exit 1 if an invalid gitlab-releaser.json file is provided', () => {
        expect.hasAssertions();
        const testFileName =
            'gitlab-releaser-invalid-default-extra-property.json';

        invalidInputTest(
            {
                ...defaultGitLabReleaserCliOptions,
                releaseFileName: testFileName
            },
            `"${path.join(
                gitlabReleaserTestFileDirectory,
                testFileName
            )}" is an invalid gitlab-releaser file`
        );
    });

    it('should log error and exit 1 if a release is not specified', () => {
        expect.hasAssertions();
        const errorMessage = 'Release must be specified';

        invalidInputTest(
            { ...defaultGitLabReleaserCliOptions, releaseName: '' },
            errorMessage
        );
    });

    it('should log error and exit 1 if a CHANGELOG path is not specified and release uses CHANGELOG data', () => {
        expect.hasAssertions();
        const errorMessage = `Changelog path not specified, but required to process changelog data`;

        invalidInputTest(
            { ...defaultGitLabReleaserCliOptions, changelogPath: undefined },
            errorMessage
        );
    });

    it('should log error and exit 1 if an invalid CHANGELOG file name is specified and release uses CHANGELOG data', () => {
        expect.hasAssertions();
        const changelogPath = 'foo/bar.md';
        const errorMessage = `Invalid changelog path "${changelogPath}", no file or directory found`;

        invalidInputTest(
            { ...defaultGitLabReleaserCliOptions, changelogPath },
            errorMessage
        );
    });

    it('should log error and exit 1 if CHANGELOG description is empty and release uses CHANGELOG data', () => {
        expect.hasAssertions();
        const releaseName = '1.0.0';
        const changelogPath = emptyChangelogPath;
        const errorMessage = `No description found for release name "${releaseName}"`;

        invalidInputTest(
            { ...defaultGitLabReleaserCliOptions, changelogPath, releaseName },
            errorMessage
        );
    });
});

describe('save release file', () => {
    const defaultDirectory = '.gitlab';
    const defaultFileName = 'release.json';
    const defaultReleaseData = {
        name: 'This is a test'
    };

    const saveFileTest = (directory, fileName, releaseData) => {
        const saveFileSpy = jest
            .spyOn(fs, 'writeFileSync')
            .mockImplementation(() => {});
        const consoleLogSpy = jest
            .spyOn(console, 'log')
            .mockImplementation(() => {});
        const consoleErrorSpy = jest
            .spyOn(console, 'error')
            .mockImplementation(() => {});
        const processExitSpy = jest
            .spyOn(process, 'exit')
            .mockImplementation(() => {});

        saveReleaseFile(directory, fileName, releaseData);

        return {
            consoleErrorSpy,
            consoleLogSpy,
            processExitSpy,
            saveFileSpy
        };
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should save file with the specified file name', () => {
        expect.assertions(1);
        const { saveFileSpy } = saveFileTest(
            defaultDirectory,
            defaultFileName,
            defaultReleaseData
        );
        expect(saveFileSpy.mock.calls[0][0]).toStrictEqual(
            path.join(defaultDirectory, defaultFileName)
        );
    });

    it('should save file with the specified release data', () => {
        expect.assertions(1);
        const expectedFileData = JSON.stringify(defaultReleaseData);
        const { saveFileSpy } = saveFileTest(
            defaultDirectory,
            defaultFileName,
            defaultReleaseData
        );
        expect(saveFileSpy.mock.calls[0][1]).toStrictEqual(expectedFileData);
    });

    it('should log that a file was saved and the file name', () => {
        expect.assertions(3);
        const { consoleLogSpy } = saveFileTest(
            defaultDirectory,
            defaultFileName,
            defaultReleaseData
        );
        expect(consoleLogSpy.mock.calls[0][0]).toMatch('Saved file');
        expect(consoleLogSpy.mock.calls[0][0]).toMatch(defaultDirectory);
        expect(consoleLogSpy.mock.calls[0][0]).toMatch(defaultFileName);
    });

    it('should log error and exit 1 if directory not specified', () => {
        expect.assertions(2);
        const { consoleErrorSpy, processExitSpy } = saveFileTest(
            undefined,
            defaultFileName,
            defaultReleaseData
        );
        expect(consoleErrorSpy.mock.calls[0][0]).toMatch('path');
        expect(processExitSpy).toHaveBeenCalledWith(1);
    });

    it('should log error and exit 1 if file name not specified', () => {
        expect.assertions(2);
        const { consoleErrorSpy, processExitSpy } = saveFileTest(
            defaultDirectory,
            undefined,
            defaultReleaseData
        );
        expect(consoleErrorSpy.mock.calls[0][0]).toMatch('path');
        expect(processExitSpy).toHaveBeenCalledWith(1);
    });
});
