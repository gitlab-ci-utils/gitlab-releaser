'use strict';

const path = require('node:path');
const {
    getReleaseDataFromChangelog,
    usesChangelog,
    processChangelogData
} = require('../lib/changelog');

const changelogKeyword = '$$CHANGELOG';
const processExitErrorMessage = 'Process exit';

const defaultRelease = '0.5.0';
const defaultData = {
    description: 'This is a test',
    name: `v${defaultRelease}`
};
const defaultPath = '.';
const defaultOptions = {
    allowEmptyChangelogDesc: false,
    changelogPath: defaultPath,
    releaseName: defaultRelease
};
const emptyDescriptionOptions = {
    ...defaultOptions,
    changelogPath: path.join('tests', 'fixtures', 'changelogs'),
    releaseName: '1.0.0'
};

describe('uses changelog', () => {
    it('should return true if name uses changelog', () => {
        expect.assertions(1);
        const data = { ...defaultData, name: changelogKeyword };
        expect(usesChangelog(data)).toBe(true);
    });

    it('should return true if description uses changelog', () => {
        expect.assertions(1);
        const data = { ...defaultData, description: changelogKeyword };
        expect(usesChangelog(data)).toBe(true);
    });

    it('should return false if neither name nor description use changelog', () => {
        expect.assertions(1);
        expect(usesChangelog(defaultData)).toBe(false);
    });

    it('should return false if neither name nor description are provided', () => {
        expect.assertions(1);
        expect(usesChangelog({})).toBe(false);
    });
});

describe('process changelog data', () => {
    let defaultReleaseData;

    beforeAll(() => {
        // Setup mock to catch console output during test
        jest.spyOn(console, 'log').mockImplementation(() => {});
        defaultReleaseData = getReleaseDataFromChangelog(defaultOptions);
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should replace name with release name if name uses changelog', () => {
        expect.assertions(1);
        const data = { ...defaultData, name: changelogKeyword };
        // Setup mock to catch console output during test
        jest.spyOn(console, 'log').mockImplementation(() => {});

        const result = processChangelogData(data, defaultOptions);

        expect(result.name).toStrictEqual(defaultReleaseData.name);
    });

    it('should replace description with release notes if description uses changelog', () => {
        expect.assertions(1);
        const data = { ...defaultData, description: changelogKeyword };
        // Setup mock to catch console output during test
        jest.spyOn(console, 'log').mockImplementation(() => {});

        const result = processChangelogData(data, defaultOptions);

        expect(result.description).toStrictEqual(
            defaultReleaseData.description
        );
    });

    it('should return original name if name does not use changelog', () => {
        expect.assertions(1);
        // Setup mock to catch console output during test
        jest.spyOn(console, 'log').mockImplementation(() => {});

        const result = processChangelogData(defaultData, defaultOptions);

        expect(result.name).toStrictEqual(defaultData.name);
    });

    it('should return original description if description does not use changelog', () => {
        expect.assertions(1);
        // Setup mock to catch console output during test
        jest.spyOn(console, 'log').mockImplementation(() => {});

        const result = processChangelogData(defaultData, defaultOptions);

        expect(result.description).toStrictEqual(defaultData.description);
    });

    it('should throw if changelog description is empty and description uses changelog', () => {
        expect.assertions(3);
        // Setup mock to catch console output during test
        jest.spyOn(console, 'log').mockImplementation(() => {});
        const consoleErrorSpy = jest
            .spyOn(console, 'error')
            .mockImplementation(() => {});
        const processExitSpy = jest
            .spyOn(process, 'exit')
            .mockImplementation(() => {
                throw new Error(processExitErrorMessage);
            });
        const data = { ...defaultData, description: changelogKeyword };

        // Function would normally exit process on error, but throws because of mock
        expect(() =>
            processChangelogData(data, emptyDescriptionOptions)
        ).toThrow(processExitErrorMessage);

        expect(consoleErrorSpy.mock.calls[0][0]).toMatch(
            'No description found for release'
        );
        expect(processExitSpy).toHaveBeenCalledWith(1);
    });

    it('should not throw if changelog description is empty and description does not use changelog', () => {
        expect.assertions(2);
        // Setup mock to catch console output during test
        jest.spyOn(console, 'log').mockImplementation(() => {});
        const processExitSpy = jest
            .spyOn(process, 'exit')
            .mockImplementation(() => {
                throw new Error(processExitErrorMessage);
            });

        // Function would normally exit process on error, but throws because of mock
        expect(() =>
            processChangelogData(defaultData, emptyDescriptionOptions)
        ).not.toThrow();
        expect(processExitSpy).not.toHaveBeenCalled();
    });

    it('should not throw if changelog description is empty, description uses changelog, and empty changelog allowed', () => {
        expect.assertions(2);
        // Setup mock to catch console output during test
        jest.spyOn(console, 'log').mockImplementation(() => {});
        const processExitSpy = jest
            .spyOn(process, 'exit')
            .mockImplementation(() => {
                throw new Error(processExitErrorMessage);
            });

        // Function would normally exit process on error, but throws because of mock
        expect(() =>
            processChangelogData(defaultData, {
                ...emptyDescriptionOptions,
                allowEmptyChangelogDesc: true
            })
        ).not.toThrow();
        expect(processExitSpy).not.toHaveBeenCalled();
    });
});

describe('get release data from changelog', () => {
    let consoleErrorSpy;
    let processExitSpy;
    let consoleLogSpy;

    beforeEach(() => {
        consoleErrorSpy = jest
            .spyOn(console, 'error')
            .mockImplementation(() => {});
        consoleLogSpy = jest.spyOn(console, 'log').mockImplementation(() => {});
        processExitSpy = jest.spyOn(process, 'exit').mockImplementation(() => {
            throw new Error(processExitErrorMessage);
        });
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should return object with name and description if changelog found', () => {
        expect.assertions(3);
        const changelogData = getReleaseDataFromChangelog(defaultOptions);
        expect(changelogData).toStrictEqual(
            expect.objectContaining({
                description: expect.any(String),
                name: expect.any(String)
            })
        );
        expect(consoleLogSpy.mock.calls[0][0]).toMatch('Found changelog');
        expect(consoleLogSpy.mock.calls[1][0]).toMatch('Found release');
    });

    it('should use changelog path if it is a valid file', () => {
        expect.assertions(1);
        const changelogFileName = 'CHANGELOG.md';
        getReleaseDataFromChangelog({
            ...defaultOptions,
            changelogPath: changelogFileName
        });
        expect(consoleLogSpy.mock.calls[0][0]).toMatch(
            `Found changelog file "${changelogFileName}"`
        );
    });

    it('should log error and exit 1 if release name not specified', () => {
        expect.assertions(3);

        // Function would normally exit process on error, but throws because of mock
        expect(() =>
            getReleaseDataFromChangelog({
                ...defaultOptions,
                releaseName: undefined
            })
        ).toThrow(processExitErrorMessage);

        expect(consoleErrorSpy.mock.calls[0][0]).toMatch(
            'Release name not specified'
        );
        expect(processExitSpy).toHaveBeenCalledWith(1);
    });

    it('should log error and exit 1 if changelog path not specified', () => {
        expect.assertions(3);

        // Function would normally exit process on error, but throws because of mock
        expect(() =>
            getReleaseDataFromChangelog({
                ...defaultOptions,
                changelogPath: undefined
            })
        ).toThrow(processExitErrorMessage);

        expect(consoleErrorSpy.mock.calls[0][0]).toMatch(
            'Changelog path not specified'
        );
        expect(processExitSpy).toHaveBeenCalledWith(1);
    });

    it('should log error and exit 1 if changelog file specified and not found', () => {
        expect.assertions(3);

        // Function would normally exit process on error, but throws because of mock
        expect(() =>
            getReleaseDataFromChangelog({
                ...defaultOptions,
                changelogPath: 'not-changelog.md'
            })
        ).toThrow(processExitErrorMessage);

        expect(consoleErrorSpy.mock.calls[0][0]).toMatch(
            'Invalid changelog path'
        );
        expect(processExitSpy).toHaveBeenCalledWith(1);
    });

    it('should log error and exit 1 if changelog not found in path', () => {
        expect.assertions(3);

        // Function would normally exit process on error, but throws because of mock
        expect(() =>
            getReleaseDataFromChangelog({
                ...defaultOptions,
                changelogPath: 'schemas'
            })
        ).toThrow(processExitErrorMessage);

        expect(consoleErrorSpy.mock.calls[0][0]).toMatch('Changelog not found');
        expect(processExitSpy).toHaveBeenCalledWith(1);
    });

    it('should log error and exit 1 if release not found in changelog', () => {
        expect.assertions(3);

        // Function would normally exit process on error, but throws because of mock
        expect(() =>
            getReleaseDataFromChangelog({
                ...defaultOptions,
                releaseName: '0.1.0'
            })
        ).toThrow(processExitErrorMessage);

        expect(consoleErrorSpy.mock.calls[0][0]).toMatch(
            /Release name .* not found/
        );
        expect(processExitSpy).toHaveBeenCalledWith(1);
    });
});
