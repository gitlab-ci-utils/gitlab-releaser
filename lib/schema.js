'use strict';

/**
 * Functions for working with gitlab-releaser and release schemas.
 *
 * @module schema
 */

const fs = require('node:fs');
const path = require('node:path');
const Ajv = require('ajv').default;

const schemaDirectory = 'schemas';
const gitlabReleaserSchemaFile = 'gitlab-releaser.schema.json';

/**
 * Common function to validate data against a JSON schema, includes
 * cases with child schemas that must be loaded.
 *
 * @param   {object}  data The data to validate against the schema.
 * @returns {boolean}      True if the data is valid for the given
 *                         schema, otherwise false.
 * @private
 */
const validateSchema = (data) => {
    const schema = JSON.parse(
        // Values specified above.
        // nosemgrep: eslint.detect-non-literal-fs-filename
        fs.readFileSync(
            // nosemgrep: path-join-resolve-traversal, detect-non-literal-fs-filename
            path.join(
                __dirname,
                '..',
                schemaDirectory,
                gitlabReleaserSchemaFile
            )
        )
    );
    const validate = new Ajv().compile(schema);
    return validate(data);
};

module.exports.validateSchema = validateSchema;
