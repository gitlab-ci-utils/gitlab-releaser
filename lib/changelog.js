'use strict';

/**
 * Functions for working with CHANGELOG files.
 *
 * @module changelog
 */

const fs = require('node:fs');
const logger = require('ci-logger');
const releaselog = require('releaselog');

/**
 * The keyword used in gitlab-releaser.json to indicate that the release name
 * and/or description should be retrieved from the CHANGELOG.
 *
 * @type {string}
 * @static
 * @public
 */
const changelogKeyword = '$$CHANGELOG';

/**
 * Checks a release to determine whether it uses CHANGELOG data
 * for release name or description.
 *
 * @param   {object}  data The release object to check.
 * @returns {boolean}      True if the release uses CHANGELOG data,
 *                         otherwise false.
 * @static
 * @public
 */
const usesChangelog = (data) =>
    data.name === changelogKeyword || data.description === changelogKeyword;

// eslint-disable-next-line jsdoc/require-returns-check -- exits instead of returning
/**
 * Checks a path, which could be a file or directory, to determine the
 * CHANGELOG file path. If the path is a valid file path, it is returned. If
 * it is a directory, it is checked using releaselog to find the appropriate
 * CHANGELOG. If the path does not exist, or a CHANGELOG is not found,
 * it will log error and exit.
 *
 * @param   {string} path The path to check.
 * @returns {string}      The full path to the CHANGELOG.
 * @static
 * @private
 */
// eslint-disable-next-line consistent-return -- Logger logs and exits on error
const getChangelogFilePath = (path) => {
    try {
        // User inputs to dev tool, no limits on file path.
        // nosemgrep: detect-non-literal-fs-filename, eslint.detect-non-literal-fs-filename
        const stats = fs.statSync(path);
        const changelogFileName = stats.isFile()
            ? path
            : releaselog.findChangelog(path);
        if (changelogFileName) {
            logger.log({
                message: `Found changelog file "${changelogFileName}"`
            });
            return changelogFileName;
        }
        logger.log({
            level: logger.Levels.Error,
            message: `Changelog not found in path "${path}"`
        });
    } catch {
        logger.log({
            level: logger.Levels.Error,
            message: `Invalid changelog path "${path}", no file or directory found`
        });
    }
};

/* eslint-disable max-lines-per-function, consistent-return --
   max-lines-per-function - exceeds due to formatting,
   consistent-return - Logger logs and exits on error */
// eslint-disable-next-line jsdoc/require-returns-check -- exits instead of returning
/**
 * Gets release data from a CHANGELOG file. Will log error and exit
 * process if no CHANGELOG is found or the release cannot be found in
 * the CHANGELOG.
 *
 * @param   {object} options               The options used to create the release.
 * @param   {string} options.releaseName   The name to use to find release data
 *                                         in the CHANGELOG and gitlab-releaser file.
 * @param   {string} options.changelogPath The path to the CHANGELOG directory or file.
 * @returns {object}                       The release name/description from
 *                                         the CHANGELOG.
 * @static
 * @public
 */
const getReleaseDataFromChangelog = ({ releaseName, changelogPath }) => {
    if (!releaseName) {
        logger.log({
            level: logger.Levels.Error,
            message:
                'Release name not specified, but required to process changelog'
        });
    }
    if (!changelogPath) {
        logger.log({
            level: logger.Levels.Error,
            message:
                'Changelog path not specified, but required to process changelog data'
        });
    }
    const changelog = getChangelogFilePath(changelogPath);
    const releaseDataFromChangelog = releaselog.getReleaseDetails(
        changelog,
        releaseName
    );
    if (releaseDataFromChangelog) {
        logger.log({
            message: `Found release "${releaseDataFromChangelog.title}" in "${changelog}"`
        });
        return {
            description: releaseDataFromChangelog.notes,
            name: releaseDataFromChangelog.title
        };
    }
    logger.log({
        level: logger.Levels.Error,
        message: `Release name "${releaseName}" not found in changelog "${changelog}"`
    });
};
/* eslint-enable max-lines-per-function, consistent-return -- re-enable */

/**
 * Gets applicable CHANGELOG data and returns the updated release.
 *
 * @param   {object}  data                            The release.
 * @param   {object}  options                         The options used to create the release.
 * @param   {string}  options.releaseName             The name to use to find release data
 *                                                    in the CHANGELOG and gitlab-releaser file.
 * @param   {string}  options.changelogPath           The path to the CHANGELOG directory or file.
 * @param   {boolean} options.allowEmptyChangelogDesc Allow an empty description from the CHANGELOG.
 * @returns {object}                                  The release updated with CHANGELOG data.
 * @static
 * @public
 */
const processChangelogData = (data, options) => {
    const changelogData = getReleaseDataFromChangelog(options);
    const processedData = data;
    if (data.name === changelogKeyword) {
        logger.log({ message: 'Retrieved release name from changelog' });
        processedData.name = changelogData.name;
    }
    if (data.description === changelogKeyword) {
        if (!changelogData.description && !options.allowEmptyChangelogDesc) {
            logger.log({
                level: logger.Levels.Error,
                message: `No description found for release name "${options.releaseName}"`
            });
        }
        logger.log({ message: 'Retrieved release description from changelog' });
        processedData.description = changelogData.description;
    }
    return processedData;
};

module.exports.getReleaseDataFromChangelog = getReleaseDataFromChangelog;
module.exports.processChangelogData = processChangelogData;
module.exports.usesChangelog = usesChangelog;
module.exports.changelogKeyword = changelogKeyword;
