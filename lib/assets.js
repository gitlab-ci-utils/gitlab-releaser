'use strict';

/**
 * Functions for manipulating asset data.
 *
 * @module assets
 */

/**
 * Release asset link object.
 *
 * @typedef  {object} AssetLink
 * @property {string} name                - The name of the asset.
 * @property {string} url                 - The URL of the asset.
 * @property {string} [type]              - The type of the asset.
 * @property {string} [direct_asset_path] - The direct asset path.
 */

/**
 * Convert the assetsLink object to a serialized JSON string.
 *
 * @param   {AssetLink} assetLink The asset link object.
 * @returns {string}              The formatted asset link JSON string.
 * @throws  {TypeError}           If no asset link provided.
 * @static
 * @public
 */
const formatAssetLink = (assetLink) => {
    if (assetLink) {
        // eslint-disable-next-line camelcase -- match API
        const { name, url, type, direct_asset_path } = assetLink;

        const link = {
            name,
            url
        };

        if (type) {
            link.type = type;
        }

        // eslint-disable-next-line camelcase -- match API
        if (direct_asset_path) {
            // eslint-disable-next-line camelcase -- match API
            link.direct_asset_path = direct_asset_path;
        }

        return JSON.stringify(link);
    }
    throw new TypeError('No asset link provided.');
};

module.exports.formatAssetLink = formatAssetLink;
