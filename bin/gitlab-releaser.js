#!/usr/bin/env node
'use strict';

const { program } = require('commander');
const env = require('gitlab-ci-env');
const pkg = require('../package.json');
const { getReleaseData, saveReleaseFile } = require('..');

const releaseDirectory = '.gitlab';
const releaseFileName = 'gitlab-releaser.json';
const releaseScriptName = 'release.json';

program
    .version(pkg.version)
    .option(
        '-r, --release <release>',
        'the reference used to retrieve release data (default: "$CI_COMMIT_TAG")',
        env.ci.commit.tag
    )
    .option(
        '-c, --changelog <changelog>',
        'the path to the CHANGELOG to check if the release pulls data from the ' +
            'CHANGELOG',
        '.'
    )
    .option(
        '-a, --allow-empty-changelog-desc',
        'allow an empty description when pulled from the CHANGELOG',
        false
    )
    .option(
        '-v, --allow-empty-variables',
        'allow environment variables that are empty and are not expanded',
        false
    )
    .parse(process.argv);
const options = program.opts();

const releaseCliOptions = {
    allowEmptyChangelogDesc: options.allowEmptyChangelogDesc,
    allowEmptyVariables: options.allowEmptyVariables,
    changelogPath: options.changelog,
    releaseDirectory,
    releaseFileName,
    releaseName: options.release
};

const releaseData = getReleaseData(releaseCliOptions);
saveReleaseFile(releaseDirectory, releaseScriptName, releaseData);
