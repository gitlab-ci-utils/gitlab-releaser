# Changelog

## v8.0.6 (2025-01-21)

### Fixed

- Updated to latest dependencies (`commander@13.1.0`, `gitlab-ci-env@10.2.0`,
  `releaselog@6.0.3`).

## v8.0.5 (2025-01-04)

### Fixed

- Updated to latest dependencies (`commander@13.0.0`, `gitlab-ci-env@10.1.0`,
  `releaselog@6.0.2`).

## v8.0.4 (2024-07-13)

### Fixed

- Updated to latest dependencies (`ajv@8.17.1`).

## v8.0.3 (2024-07-05)

### Fixed

- Updated output release file to rename `released_at` property to `released-at`
  to match expected `release-cli create-from-file` schema. (#65)
  - This schema is incorrectly captured in the `release-cli` test file (See
    [this issue](https://gitlab.com/gitlab-org/release-cli/-/issues/191)).

## v8.0.2 (2024-07-02)

### Fixed

- Updated output release file to rename `milestones` property to `milestone`
  to match expected `release-cli create-from-file` schema. (#64)
  - This schema is incorrectly captured in the `release-cli` test file (See
    [this issue](https://gitlab.com/gitlab-org/release-cli/-/issues/191)).
  - The `gitlab-releaser` schema was left as `milestones` for compatibility
    and is more consistent with the other properties (some of which differ
    from the `release-cli` schema).
- Updated to latest dependencies (`releaselog@6.0.1`).

## v8.0.1 (2024-06-09)

### Fixed

- Updated to latest dependencies (`ajv@8.16.0`, `commander@12.1.0`,
  `gitlab-ci-env@10.0.0`).

### Miscellaneous

- Update to Renovate config v1.1.0.

## v8.0.0 (2024-02-05)

### Changed

- BREAKING: Updated `gitlab-releaser` JSON schema replacing `filepath` with
  `direct_asset_path` per changes to the GitLab API. (#58)
  - Any existing files using `filepath` must be changed to `direct_asset_path`. See example
    [here](tests\fixtures\gitlab-releaser\gitlab-releaser-valid-all-values.json).
- BREAKING: Deprecated support for Node 21 (end-of-life 2024-06-01) and added
  support for Node 22 (released 2024-04-25). Compatible with all current and
  LTS releases (`^18.12.0 || ^20.9.0 || >=22.0.0`). (#62)

### Fixed

- Updated to latest dependencies (`ajv@8.13.0`, `ci-logger@7.0.0`, `gitlab-ci-env@9.3.0`,
  `releaselog@6.0.0`).

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#61)

## v7.0.2 (2024-02-05)

### Fixed

- Updated to latest dependencies (`commander@12.0.0`, `gitlab-ci-env@9.2.0`,
  `releaselog@5.0.1`).

## v7.0.1 (2023-11-08)

### Fixed

- Updated variable expansion logic to properly handle multiple variables in
  a single property value. (#59)

## v7.0.0 (2023-11-07)

### Changed

- BREAKING: Deprecated support for Node 16 (end-of-life 2023-09-11) and added
  support for Node 21 (released 2023-10-17). Compatible with all current and
  LTS releases (`^18.12.0 || >=20.0.0`). (#54, #55)

### Fixed

- Fixed error in variable expansion logic where variables in `assets` were not
  being expanded. (#59)
- Updated to latest dependencies (`commander@11.1.0`, `releaselog@5.0.0`)

### Miscellaneous

- Updated package to publish to NPM with
  [provenance](https://github.blog/2023-04-19-introducing-npm-package-provenance/).

## v6.0.0 (2023-09-24)

### Changed

- BREAKING: Changed output from shell script to JSON file compatible with
  new `release-cli create-from-file` command. (#56)
  - The previous `.gitlab/release.json` files as input are no longer supported
    and must be converted to `gitlab-releaser` files. See the
    [README](./README.md) for details (add release under `defaults`). This old
    schema differs from the new schema supported by `release-cli`, so this was
    removed to avoid confusion.
    - Because of this, the `--schema` CLI option has also been removed.
  - Environment variables in release properties are now expanded and included
    in the `release.json` file rather than being included in the shell script
    and expanded when the release is created. If a variable to be expanded is
    not set, `gitlab-releaser` will fail. There is a new CLI option to allow
    empty variables if desired (`--allow-empty-variables`). In this case the
    variables are not expanded. (#57)
  - Exported functions changed names from
    `getReleaseCliCommand`/`saveReleaseCliScript` to
    `getReleaseData`/`saveReleaseFile`.
  - See the [README](./README.md) for changes to GitLab CI jobs.
  - Requires `release-cli` v0.16.0 or later.
- BREAKING: Deprecated support for Node 14 (end-of-life 2023-04-30), Node 16
  (end-of-life 2023-09-11), and 19 (end-of-life 2023-06-01). Compatible with
  all current and LTS releases (`^18.12.0 || >=20.0.0`). (#50)
- BREAKING: Changed package `main` to `exports` with only the default export.
  Only breaking if an internal module is imported.

### Fixed

- Updated examples to use the `variables:expand` option, added in
  GitLab 15.8. (#48)
- Updated to latest dependencies (`ci-logger@6.0.0`, `commander@11.0.0`, `gitlab-ci-env@9.0.0`, `releaselog@4.0.1`)
- Moved from `markdownlint-cli` to `markdownlint-cli2`.

### Miscellaneous

- Updated to publish to NPM with provenance.
- Updated Renovate config for v36.

## v5.0.1 (2023-03-26)

### Fixed

- Updated to latest dependencies (`ci-logger@5.1.1`, `releaselog@^3.0.4`).

## v5.0.0 (2023-01-30)

### Changed

- BREAKING: Update changelog processing to report error and exit if the
  changelog description is used, but the description that was found is empty.
  This is almost always a changelog formatting or content issue. (#47)
- Add CLI option `-c`/`--changelog` to specify the path to the changelog,
  which can be a file or a directory. (#42)
- BREAKING: Refactored the `getReleaseCliCommand` function to accept an
  `options` object with all options. This is only breaking if that
  function is used programmatically and not via the CLI. (#47)

### Fixed

- Updated to latest dependencies (`ajv@8.12.0`, `commander@10.0.0`,
  `releaselog@3.0.3`)

## v4.0.4 (2023-01-01)

### Fixed

- Updated to latest dependencies (`ajv@8.11.2`, `gitlab-ci-env@7.0.0`)

## v4.0.3 (2022-10-30)

### Fixed

- Updated to latest dependencies (`commander@9.4.1`, `gitlab-ci-env@6.4.0`, `releaselog@3.0.2`)

### Miscellaneous

- Updated pipeline to have long-running jobs use GitLab shared `medium` sized runners. (#46)

## v4.0.2 (2022-08-28)

### Fixed

- Updated to latest dependencies (`ci-logger` to `^5.1.0`, `gitlab-ci-env` to `^6.2.2`)
- Refactored input file schema tests to reduce duplication and improve extensibility. (#45)
- Refactored tests for breaking changes in `jest@29.0.1`

## v4.0.1 (2022-07-31)

### Fixed

- Updated to latest dependencies

## v4.0.0 (2022-05-30)

### Changed

- BREAKING: Deprecated support for Node 12 and 17 since end-of-life. Compatible with all current and LTS releases (`^14.15.0 || ^16.13.0 || >=18.0.0`). (#40, #41)

### Fixed

- Refactored changelog processing to reference release name instead of tag for clarity. (#44, #35)

### Miscellaneous

- Added `prettier` and disabled `eslint` formatting rules. (#39)
- Added test coverage threshold requirements (#43)

## v3.0.0 (2022-03-27)

### Changed

- BREAKING: Update application to cover the cases where the release name in a gitlab-releaser.json or CHANGELOG are different from the git tag. (#35)
  - Changed CLI argument `-t`/`--tag` to `-r`/`--release` to reflect how this value is now used, which is only to apply release-specific data from a gitlab-releaser.json file or to retrieve values from a CHANGELOG. The default value is still `$CI_COMMIT_TAG`. Any scripts using the `-t`/`--tag` arguments must be updated to `-r`/`--release`.
  - The `--release` value no longer overrides a `tag_name` in the release script. If a `tag_name` value is specified in a release.json or gitlab-releaser.json file it will be included in the resulting release script, otherwise no `tag_name` will be included (in which case GitLab release-cli will use its default value of `$CI_COMMIT_TAG`).
  - Thanks to @reggie-k for identifying this use case.

### Fixed

- Refactored to comply with latest `eslint` rules
- Updated to latest dependencies, including resolving CVE-2021-44906 (dev only)
- Update jsdoc comments for consistency and completeness (#10)

### Miscellaneous

- Registered project for [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/5747/badge)](https://bestpractices.coreinfrastructure.org/projects/5747) badge
- Updated project policy documents (CODE_OF_CONDUCT.md, CONTRIBUTING.md, SECURITY.md) (#37)

## v2.0.4 (2022-01-23)

### Fixed

- Updated to latest dependencies

## v2.0.3 (2021-11-21)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Refactored tests for per updated lint rules

## v2.0.2 (2021-09-19)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Setup [renovate](https://docs.renovatebot.com/) for dependency updates (#30)

## v2.0.1 (2021-07-06)

### Fixed

- Updated to latest dependencies (#28)

## v2.0.0 (2021-06-06)

### Changed

- BREAKING: Updated command line arguments to only require a tag to be specified if it is necessary to process the command. This includes retrieving the release data if the schema is `gitlab-releaser` or if the file references CHANGELOG data. (#23)
- BREAKING: Updated the output shell script to include the `--tag-name` argument if a tag is specified in the release file or the CLI (previously it was only included if in the release file). If a tag is specified in both places, the CLI argument takes precedence. (#23)
- BREAKING: Updated `release` schema to allow zero milestones. This facilitates the case a milestone is defined in `defaults` in `gitlab-releaser`, but need to override a specific release to have no milestones, which can now be done by specifying an empty array. (#15)
- BREAKING: Deprecated support for Node 10 and 15 since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#21)

### Fixed

- Fixed incorrect description for the `--tag`/`-t` CLI argument (#27)
- Updated to latest dependencies

### Miscellaneous

- Updated documentation with an example GitLab CI job template that includes a default release configuration that will only be used if no release file is present. (#26)
- Update `gitlab-releaser` schema to reference `release` schema, rather than repeat it. (#22)

## v1.0.4 (2021-05-12)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Optimized published package to only include the minimum required files (#20)

## v1.0.3 (2021-03-27)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated pipeline to use standard NPM package collection (#18)

## v1.0.2 (2021-02-27)

### Fixed

- Updated to latest dependencies (#16)

### Miscellaneous

- Update CI pipeline to check for secure JSON schemas and resolved several issues (#14)
- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#11) and GitLab Releaser template (#12)
- Update SBOM type to application (#17)

## v1.0.1 (2020-12-27)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v1.0.0 (2020-11-21)

### Added

- Added support for `gitlab-releaser.json` file format, which allows declaration of default values applicable to all releases and overrides for each specific release. (#4)

### Changed

- Added option for pulling release `name` and `description` from the CHANGELOG (#2)
- Updated argument formatting in shell script to assume string literal values, but allow expansion of environment variables (#3)
- Added basic console logging for successful execution (#7)

### Fixed

- Updated to latest dependencies

## v0.5.0 (2020-11-10)

Initial implementation
